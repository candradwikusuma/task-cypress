const { Random } = require('random-js');
let requestRegister;
describe('Login Lender',()=>{

    beforeEach(()=>{
        requestRegister={
            "fullname": `Fullname ${randomAlphabet()}`,
            "email": `test-${randomNumber()}@gmail.com`,
            'password':'Elmacho99'
          
        }
        cy.visit('https://investree.tech/login',{
            auth:{
                username:'investree',
                password:'investme!'
            }
        });
      

        cy.get('[data-unq="registration-lender"]').click()
        cy.get('[data-unq="btn-beneficial-owner-0"]').click()
        cy.get('[data-unq="btn-submit-beneficial-owner"]').click()
        cy.get('[data-unq="auth-registration-salutation"]').click()
        cy.get('[data-unq="auth-registration-salutation-options-0"]').click()
        cy.get('[data-unq="auth-registration-fullname"]').type(requestRegister.fullname)
        cy.get('[data-unq="auth-registration-email"]').type(requestRegister.email)
        cy.get('[data-unq="auth-registration-password"]').type(requestRegister.password)
        cy.get('[data-unq="auth-registration-agree-privacy"]').check()
        cy.get('[data-unq="auth-registration-agreeSubscribe"]').check()
        cy.get('[btn-unq="auth-registration-btn-submit"]').click()

        cy.get('[data-unq="btn-open-email"]').should('contain','Buka Email')
      
        cy.visit('https://investree.tech/v3/auth/logout',{
            auth:{
                username:'investree',
                password:'investme!'
            }
        });
        
    })
    it('Login Lender with valid credential should success',function(){
        cy.visit('https://investree.tech/login',{
            auth:{
                username:'investree',
                password:'investme!'
            }
        });
       
         cy.get('[data-unq="email"]').type(requestRegister.email)
         cy.get('[data-unq="password"]').type(requestRegister.password)
         cy.get('[data-unq="btn-submit"]').click();
         
        cy.get('[data-unq="btn-open-email"]').should('contain','Buka Email')
    })

    it('Login Lender with invalid email should message info error',function(){
        cy.visit('https://investree.tech/login',{
            auth:{
                username:'investree',
                password:'investme!'
            }
        });
       
        cy.get('[data-unq="email"]').type(`${Math.random().toString(36)}@gmail.com`)
        cy.get('[data-unq="password"]').type(requestRegister.password)
         cy.get('[data-unq="btn-submit"]').click();

         cy.get('[data-unq="error-msg"]').should('have.class','alert text-center alert-danger')
         
    })
})

function randomNumber(length = 9) {
    const pool = '1234567890';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomNum = new Random().integer(1, 9);
        result = randomNum + result.slice(1, result.length);
    }

    return result;
}

function randomAlphabet(length = 12) {
    const pool = 'abcdefghijklmnopqrstuvwxyz';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomAlpha = new Random().string(1, 25);
        result = randomAlpha + result.slice(1, result.length);
    }

    return result;
}